<?php
namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function index() {
        return response()->json(Module::all(), 200);
    }

    public function store(Request $request){
        $module = new Module();
        $module->fill($request->all());
        $module->save();
        return response()->json($module, 200);
    }

    public function update(Request $request, $id){
        $module = Module::find($id);
        $module->fill($request->all());
        $module->save();
        return response()->json($module, 200);
    }

    public function destroy($id){
        Module::destroy($id);
        return response()->json([], 200);
    }

    public function show($id){
        $module = Module::find($id);
        $module->instituition;
        return response()->json($module, 200);
    }

}