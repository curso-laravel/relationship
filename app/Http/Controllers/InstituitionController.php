<?php
namespace App\Http\Controllers;

use App\Models\Instituition;
use Illuminate\Http\Request;

class InstituitionController extends Controller
{
    public function index() {
        return response()->json(Instituition::all(), 200);
    }

    public function store(Request $request){
        $instituition = new Instituition();
        $instituition->fill($request->all());
        $instituition->save();
        return response()->json($instituition, 200);
    }

    public function update(Request $request, $id){
        $instituition = Instituition::find($id);
        $instituition->fill($request->all());
        $instituition->save();
        return response()->json($instituition, 200);
    }

    public function destroy($id){
        Instituition::destroy($id);
        return response()->json([], 200);
    }

    public function show($id){
        $instituition = Instituition::find($id);
        return response()->json($instituition, 200);
    }

}