<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['name', 'instituition_id'];

    public function instituition() {
        return $this->hasOne('App\Models\Instituition', 'id', 'instituition_id');
    }

}
